<?php

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";
require_once "phar://wis_sass_framework.phar/build.php";

CDispatcher::AddParamVariable('WIS_SASS_FRAMEWORK_VERSION', "1.0.1 built on $WIS_SASS_FRAMEWORK_BUILT_DATE (MM/DD/YYYY)");

$ONIX_PATCH_MODEL = 'MSassPatchHistory';
$ONIX_PATCH_PHP = "phar://wis_sass_framework.phar/models/MSassPatchHistory.php";
$ONIX_SQL_PATH = "phar://wis_sass_framework.phar/patch";

$ONIX_PATCH_LIST = [    
    ['0.0', 'WIS_SASS_0.0.20180511.sql'],   
    ['0.1', 'WIS_SASS_0.1.20180905.sql'],
];

$ONIX_CUSTOM_PATCH_LIST = [    
];

$ONIX_POST_PATCH_LIST = [
];

//Is being used in GetStaticPatchHistoryList()
$_ENV['ONIX_PATCH_LIST'] = $ONIX_PATCH_LIST;

CPatchDB::RegisterExtraPatch($ONIX_PATCH_LIST, $ONIX_PATCH_MODEL, $ONIX_PATCH_PHP, $ONIX_SQL_PATH, $ONIX_CUSTOM_PATCH_LIST, $ONIX_POST_PATCH_LIST);

?>
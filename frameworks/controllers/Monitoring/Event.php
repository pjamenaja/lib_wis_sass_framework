<?php
/* 
    Purpose : Controller for Bill SImulate
    Created By : Seubpong Monsar
    Created Date : 05/14/2018 (MM/DD/YYYY)
    IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class Event extends CBaseController
{
    private static $cfg = NULL;

    private static $orderByConfig = [
    ];

    private static function initSqlConfig($db)
    {
        $config = [
            //Array name, model, query ind, insert/update/delete ind, delete-by-parent ind
        ];

        self::$cfg = $config;

        return($config);
    }

    private static function createObject($db)
    {
        $u = new MEvent($db);
        return($u);
    }

    public static function SassGetEventList($db, $param, $data)
    {
        $u = self::createObject($db);

        CHelper::OverrideOrderBy($u, 0, $data, self::$orderByConfig);
        list($cnt, $item_cnt, $chunk_cnt, $rows) = $u->QueryChunk(0, $data);

        $pkg = new CTable($u->GetTableName());
        self::PopulateRow($pkg, $item_cnt, $chunk_cnt, 'EVENT_LIST', $rows);
        
        return(array($param, $pkg));
    }

    public static function SassGetEventInfo($db, $param, $data)
    {
        //self::SetDumpSQL(true);
        $cfg = self::initSqlConfig($db);

        $u = self::createObject($db);
        $obj = self::GetRowByID($data, $u, 0);

        if (!isset($obj))
        {
            throw new Exception("No event in database!!!");
        }

        self::PopulateChildItems($obj, $u, $cfg);

        return(array($param, $obj));        
    }

    public static function SassCreateEvent($db, $param, $data)
    {
        $u = self::createObject($db);

        $childs = self::initSqlConfig($db);
        self::CreateData($db, $data, $u, 0, $childs);

        return(array($param, $data));        
    }    

    public static function SassUpdateEvent($db, $param, $data)
    {
        $u = self::createObject($db);
        self::updateDefaultFields($data);
        
        $childs = self::initSqlConfig($db);
        self::UpdateData($db, $data, $u, 0, $childs);

        return(array($param, $data));        
    }      

    public static function SassDeleteEvent($db, $param, $data)
    {
//CSql::SetDumpSQL(true);        
        $u = self::createObject($db);

        $childs = self::initSqlConfig($db);
        self::DeleteData($db, $data, $u, 0, $childs);

        return(array($param, $data));
    }

    function SassExtractAndCreateEvent($db, $param, $data)
    {
        $server_ip = $_SERVER['REMOTE_ADDR'];

        [$paramIn, $tableIn] = $data->getChildArray('EVENT_IN_ARR');
        $xmlIn = CUtils::CreateResultXML($paramIn, $tableIn);

        [$paramOut, $tableOut] = $data->getChildArray('EVENT_OUT_ARR');
        $xmlOut = CUtils::CreateResultXML($paramOut, $tableOut);

        [$e, $p, $g, $s] = $data->getChildArray('EVENT_VAR_ARR');

        $evt = new CTable('EVENT');
    
        $evt->setFieldValue('API_NAME', $paramIn->getFieldValue('FUNCTION_NAME'));
        $evt->setFieldValue('SERVER_IP', $server_ip);
        $evt->setFieldValue('CLIENT_IP', $s->getFieldValue('REMOTE_ADDR'));
        $evt->setFieldValue('DURATION', $e->getFieldValue('PROCESSING_DURATION'));
        $evt->setFieldValue('CLIENT_CBU', $e->getFieldValue('CLIENT'));
        $evt->setFieldValue('CLIENT_SYTEM', $e->getFieldValue('SYSTEM'));
        $evt->setFieldValue('RETURN_CODE', $paramOut->getFieldValue('ERROR_CODE'));
        $evt->setFieldValue('ERROR_MSG', $paramOut->getFieldValue('ERROR_DESC'));
        $evt->setFieldValue('SESSION_KEY', $paramIn->getFieldValue('SESSION'));
        $evt->setFieldValue('STAGE', $e->getFieldValue('STAGE'));
        $evt->setFieldValue('PRODUCT', $e->getFieldValue('PRODUCT'));
        $evt->setFieldValue('VERSION_CORE', $paramOut->getFieldValue('ONIX_CORE_FRAMEWORK_VERSION'));
        $evt->setFieldValue('VERSION_FRAMEWORK', $paramOut->getFieldValue('ONIX_ERP_FRAMEWORK_VERSION'));
        $evt->setFieldValue('VERSION_APP', $paramOut->getFieldValue('APP_VERSION_LABEL'));

        $evt->setFieldValue('USER_NAME', $e->getFieldValue('LOGIN_USER_NAME'));
        $evt->setFieldValue('LENGTH_INPUT', strlen($xmlIn));
        $evt->setFieldValue('LENGTH_OUTPUT', strlen($xmlOut));
        $evt->setFieldValue('LENGTH_COMPRESSED_OUTPUT', $e->getFieldValue('SEND_OUT_COMPRESSED_SIZE'));
        $evt->setFieldValue('LENGTH_ENCRYPTED_OUTPUT', $e->getFieldValue('SEND_OUT_ENCRYPTED_SIZE'));
        $evt->setFieldValue('SCREEN_ID', $paramIn->getFieldValue('SCREEN_ID'));
        $evt->setFieldValue('EVENT_CONTENT_PATH', $e->getFieldValue(''));
        $evt->setFieldValue('CALLER_MODE', $e->getFieldValue('CALLER_MODE'));

        self::CreateEvent($db, $param, $evt);

        return(array($param, new CTable("EVENT")));
    }    
}

?>
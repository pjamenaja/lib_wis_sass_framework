<?php
/* 
    Purpose : Controller for Search Filtering
    Created By : Seubpong Monsar
    Created Date : 01/19/2018 (MM/DD/YYYY)
    IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class SassSearchFilter extends CBaseController
{
    private static $cfg = NULL;

    private static $NS_CONFIG = [
        #NameSpace => Model, QueryIndex, CodeField, DescField, HashConfig

        'MicroServiceCodeNS' => ["MMicroService", 0, 'SERVICE_CODE', 'SERVICE_NAME', []],
    ];

    private static function populateAdditionalFields($obj, $fldsHash)
    {
        foreach ($fldsHash as $name => $value)
        {
            $obj->SetFieldValue($name, $value);
        }
    }

    public static function SassGetSearchTextList($db, $param, $data)
    {
        $ns = $data->GetFieldValue('DESCRIPTION');

        $cfg = self::$NS_CONFIG["$ns"];
        if (!isset($cfg))
        {
            throw new Exception("Name space [$ns] not found!!!!!");
        }

        list($model, $idx, $codeName, $descName, $fieldsHash) = $cfg;

        $filter = new CTable('');
        $filter->SetFieldValue($codeName, $data->GetFieldValue('CODE'));
        self::populateAdditionalFields($filter, $fieldsHash);

        $u = new $model($db);
        $u->OverideOrderBy($idx, "ORDER BY $codeName DESC ");        
        list($cnt, $rows) = $u->Query($idx, $filter);

        $arr = [];
        foreach ($rows as $r)
        {
            $o = new CTable('');
            $o->SetFieldValue('CODE', $r->GetFieldValue($codeName));
            $o->SetFieldValue('DESCRIPTION', $r->GetFieldValue($descName));

            array_push($arr, $o);
        }

        $result = new CTable($u->GetTableName());
        self::PopulateRow($result, $cnt, 1, 'SEARCH_TEXT_LIST', $arr);
        
        return(array($param, $result));
    }
}

?>
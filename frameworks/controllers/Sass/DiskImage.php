<?php
/* 
    Purpose : Controller for DiskImage
    Created By : Seubpong Monsar
    Created Date : 09/23/2018 (MM/DD/YYYY)
    IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class DiskImage extends CBaseController
{
    private static $cfg = NULL;

    private static $orderByConfig = [
    ];

    private static function initSqlConfig($db)
    {
        $config = [
            //Array name, model, query ind, insert/update/delete ind, delete-by-parent ind
        ];

        self::$cfg = $config;

        return($config);
    }

    private static function createObject($db)
    {
        $u = new MDiskImage($db);
        return($u);
    }

    public static function SassIsDiskImageExist($db, $param, $data)
    {
        $u = self::createObject($db);
        $o = self::ValidateForDuplicate($db, $data, $u, "DISK_IMAGE_NAME", "DISK_IMAGE_NAME", 0);

        return(array($param, $o));
    }

    public static function SassGetDiskImageList($db, $param, $data)
    {
        $u = self::createObject($db);

        CHelper::OverrideOrderBy($u, 0, $data, self::$orderByConfig);
        list($cnt, $item_cnt, $chunk_cnt, $rows) = $u->QueryChunk(0, $data);

        $pkg = new CTable($u->GetTableName());
        self::PopulateRow($pkg, $item_cnt, $chunk_cnt, 'DISK_IMAGE_LIST', $rows);
        
        return(array($param, $pkg));
    }

    public static function SassGetDiskImageInfo($db, $param, $data)
    {
        //self::SetDumpSQL(true);
        $cfg = self::initSqlConfig($db);

        $u = self::createObject($db);
        $obj = self::GetRowByID($data, $u, 0);

        if (!isset($obj))
        {
            throw new Exception("No Disk Image in database!!!");
        }

        self::PopulateChildItems($obj, $u, $cfg);

        return(array($param, $obj));        
    }

    public static function SassCreateDiskImage($db, $param, $data)
    {
        $u = self::createObject($db);

        $childs = self::initSqlConfig($db);
        self::CreateData($db, $data, $u, 0, $childs);

        return(array($param, $data));        
    }    

    public static function SassUpdateDiskImage($db, $param, $data)
    {
        $u = self::createObject($db);
        
        $childs = self::initSqlConfig($db);
        self::UpdateData($db, $data, $u, 0, $childs);

        return(array($param, $data));        
    }      

    public static function SassDeleteDiskImage($db, $param, $data)
    {
//CSql::SetDumpSQL(true);        
        $u = self::createObject($db);

        $childs = self::initSqlConfig($db);
        self::DeleteData($db, $data, $u, 0, $childs);

        return(array($param, $data));
    }
}

?>
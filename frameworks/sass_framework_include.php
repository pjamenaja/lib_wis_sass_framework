<?php

/* Put only class declaration here */

require_once "phar://onix_core_framework.phar/CTable.php";
require_once "phar://onix_core_framework.phar/CUtils.php";
require_once "phar://onix_core_framework.phar/CBaseController.php";

require_once "phar://wis_sass_framework.phar/utils/CSassHelper.php";

#Controllers
require_once "phar://wis_sass_framework.phar/controllers/Monitoring/Event.php";
require_once "phar://wis_sass_framework.phar/controllers/Sass/MicroService.php";
require_once "phar://wis_sass_framework.phar/controllers/Sass/Instance.php";
require_once "phar://wis_sass_framework.phar/controllers/Sass/DiskImage.php";
require_once "phar://wis_sass_framework.phar/controllers/General/SassSearchFilter.php";

#Models
require_once "phar://wis_sass_framework.phar/models/MSassPatchHistory.php";
require_once "phar://wis_sass_framework.phar/models/MSassEvent.php";
require_once "phar://wis_sass_framework.phar/models/MMicroService.php";
require_once "phar://wis_sass_framework.phar/models/MInstance.php";
require_once "phar://wis_sass_framework.phar/models/MDiskImage.php";

?>

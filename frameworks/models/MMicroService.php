<?php
/* 
Purpose : Model for SASS_SERVICES
Created By : Seubpong Monsar
Created Date : 09/05/2018 (MM/DD/YYYY)
IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class MMicroService extends MBaseModel
{
    private $cols = array(
                  [ # 0 For query, insert, delete, update
                   'SERVICE_ID:SPK:SERVICE_ID:Y',                 
                   'SERVICE_CODE:S:SERVICE_CODE:Y',
                   'SERVICE_NAME:S:SERVICE_NAME:Y', 
                   'DOCKER_URL:S:DOCKER_URL:Y', 
                   'CURRENT_DEV_VERSION:S:CURRENT_DEV_VERSION:N', 
                   'CURRENT_TEST_VERSION:S:CURRENT_TEST_VERSION:N', 
                   'CURRENT_PROD_VERSION:S:CURRENT_PROD_VERSION:N', 

                   'CREATE_DATE:CD:CREATE_DATE:N',
                   'MODIFY_DATE:MD:MODIFY_DATE:N',                   
                  ],
    );

    private $froms = array(
                   'FROM SASS_SERVICES ',
    );

    private $orderby = array(
                   'ORDER BY SERVICE_ID DESC ',
    );

    function __construct($db) 
    {
        parent::__construct($db, 'SASS_SERVICES', 'SERVICE_ID', $this->cols, $this->froms, $this->orderby);
    }
}

?>
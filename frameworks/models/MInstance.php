<?php
/* 
Purpose : Model for SASS_INSTANCE
Created By : Seubpong Monsar
Created Date : 09/10/2018 (MM/DD/YYYY)
IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class MInstance extends MBaseModel
{
    private $cols = array(
            [ # 0 For query, insert, delete, update
                'INSTANCE_ID:SPK:INSTANCE_ID:Y',                 
                'INSTANCE_NAME:S:INSTANCE_NAME:Y',
                'PROJECT_ID:REFID:PROJECT_ID:Y', 
                'IP_ADDRESS:S:IP_ADDRESS:Y', 
                'MACHINE_TYPE:S:MACHINE_TYPE:Y', 
                'DISK_TYPE:S:DISK_TYPE:Y', 
                'DISK_IMAGE:S:DISK_IMAGE:Y', 
                'INSTANCE_STATUS:N:INSTANCE_STATUS:Y', 
                'INSTANCE_ROLE:N:INSTANCE_ROLE:N', 

                'CREATE_DATE:CD:CREATE_DATE:N',
                'MODIFY_DATE:MD:MODIFY_DATE:N',                   
            ],

            [ # 1 Get List
                'VM.INSTANCE_ID:SPK:INSTANCE_ID:Y',                 
                'VM.INSTANCE_NAME:S:INSTANCE_NAME:Y',
                'VM.PROJECT_ID:REFID:PROJECT_ID:N', 
                'VM.IP_ADDRESS:S:IP_ADDRESS:N', 
                'VM.MACHINE_TYPE:S:MACHINE_TYPE:N', 
                'VM.DISK_TYPE:S:DISK_TYPE:N', 
                'VM.DISK_IMAGE:S:DISK_IMAGE:Y', 
                'VM.INSTANCE_STATUS:N:INSTANCE_STATUS:Y', 
                'VM.INSTANCE_ROLE:N:INSTANCE_ROLE:N', 

                'PJ.PROJECT_NAME:S:PROJECT_NAME:Y',
            ],            
    );

    private $froms = array(
            'FROM SASS_INSTANCE ',

            'FROM SASS_INSTANCE VM ' .
                'LEFT OUTER JOIN PROJECT PJ ON (VM.PROJECT_ID = PJ.PROJECT_ID) ',          
    );

    private $orderby = array(
            'ORDER BY INSTANCE_ID DESC ',

            'ORDER BY VM.INSTANCE_ID DESC ',
    );

    function __construct($db) 
    {
        parent::__construct($db, 'SASS_INSTANCE', 'INSTANCE_ID', $this->cols, $this->froms, $this->orderby);
    }
}

?>
<?php
/* 
Purpose : Model for EVENTS
Created By : Seubpong Monsar
Created Date : 05/14/2018 (MM/DD/YYYY)
IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class MSassEvent extends MBaseModel
{
    private $cols = array(
                  [ # 0 For query, insert, delete, update
                   'EVENT_ID:SPK:EVENT_ID:Y', 
                   'EVENT_DATE:SD:EVENT_DATE:N',                    
                   'SERVER_IP:S:SERVER_IP:N', 
                   'CLIENT_IP:S:CLIENT_IP:N', 
                   'DURATION:NZ:DURATION:N', 
                   'CLIENT_CBU:S:CLIENT_CBU:N', 
                   'CLIENT_SYTEM:S:CLIENT_SYTEM:N', 
                   'VERSION_CORE:S:VERSION_CORE:N', 
                   'VERSION_FRAMEWORK:S:VERSION_FRAMEWORK:N', 
                   'VERSION_APP:S:VERSION_APP:N', 
                   'API_NAME:S:API_NAME:N', 
                   'RETURN_CODE:S:RETURN_CODE:N', 
                   'ERROR_MSG:S:ERROR_MSG:N', 
                   'SUBMITTED_DATE:S:SUBMITTED_DATE:N', 
                   'SESSION_KEY:S:SESSION_KEY:N', 
                   'USER_NAME:S:USER_NAME:N', 
                   'LENGTH_INPUT:NZ:LENGTH_INPUT:N', 
                   'LENGTH_OUTPUT:NZ:LENGTH_OUTPUT:N', 
                   'LENGTH_COMPRESSED_OUTPUT:NZ:LENGTH_COMPRESSED_OUTPUT:N', 
                   'LENGTH_ENCRYPTED_OUTPUT:NZ:LENGTH_ENCRYPTED_OUTPUT:N', 
                   'SCREEN_ID:S:SCREEN_ID:N', 
                   'STAGE:S:STAGE:N', 
                   'PRODUCT:S:PRODUCT:N', 
                   'EVENT_CONTENT_PATH:S:EVENT_CONTENT_PATH:N', 
                   'CALLER_MODE:S:CALLER_MODE:N', 
                   
                   'CREATE_DATE:CD:CREATE_DATE:N',
                   'MODIFY_DATE:MD:MODIFY_DATE:N'
                  ],
    );

    private $froms = array(
                   'FROM SASS_EVENTS ',
    );

    private $orderby = array(
                   'ORDER BY EVENT_ID DESC ',
    );

    function __construct($db) 
    {
        parent::__construct($db, 'SASS_EVENTS', 'EVENT_ID', $this->cols, $this->froms, $this->orderby);
    }
}

?>
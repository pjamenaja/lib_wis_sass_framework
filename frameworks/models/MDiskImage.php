<?php
/* 
Purpose : Model for SASS_DISK_IMAGE
Created By : Seubpong Monsar
Created Date : 09/25/2018 (MM/DD/YYYY)
IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class MDiskImage extends MBaseModel
{
    private $cols = array(
            [ # 0 For query, insert, delete, update
                'DISK_IMAGE_ID:SPK:DISK_IMAGE_ID:Y',                 
                'DISK_IMAGE_NAME:S:DISK_IMAGE_NAME:Y',
                'DISK_IMAGE_DESC:S:DISK_IMAGE_DESC:Y', 
                'DISK_IMAGE_STATUS:N:DISK_IMAGE_STATUS:Y', 
                'INSTANCE_ROLE:N:INSTANCE_ROLE:N', 

                'CREATE_DATE:CD:CREATE_DATE:N',
                'MODIFY_DATE:MD:MODIFY_DATE:N',                   
            ],

            [ # 1 Get List
                'DI.DISK_IMAGE_ID:SPK:DISK_IMAGE_ID:Y',                 
                'DI.DISK_IMAGE_NAME:S:DISK_IMAGE_NAME:Y',
                'DI.DISK_IMAGE_DESC:S:DISK_IMAGE_DESC:N',  
                'DI.DISK_IMAGE_STATUS:N:DISK_IMAGE_STATUS:Y', 
                'DI.INSTANCE_ROLE:N:INSTANCE_ROLE:N', 
            ],            
    );

    private $froms = array(
            'FROM SASS_DISK_IMAGE ',

            'FROM SASS_DISK_IMAGE DI ',       
    );

    private $orderby = array(
            'ORDER BY DISK_IMAGE_ID DESC ',

            'ORDER BY VM.DISK_IMAGE_ID DESC ',
    );

    function __construct($db) 
    {
        parent::__construct($db, 'SASS_DISK_IMAGE', 'DISK_IMAGE_ID', $this->cols, $this->froms, $this->orderby);
    }
}

?>
<?php
/* 
    Purpose : Util for generic purpose
    Created By : Seubpong Monsar
    Created Date : 05/14/2018 (MM/DD/YYYY)
    IBSVer : 1.0 
*/

declare(strict_types=1);

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

class CSassHelper extends CBaseController
{
    public static function OverrideOrderBy($model, $ind, $data, $orderByCfg)
    {
        //Use this map, we don't want user/hacker to pass string directory to our query
        $orderMap = ['ASC' => 'ASC', 'DESC' => 'DESC'];

        $arr = $data->GetChildArray('@ORDER_BY_COLUMNS');
        if (count($arr) <= 0)
        {
            return;
        }

        $tmp = "";
        $clause = [];

        foreach ($arr as $o)
        {
            $colKey = $o->GetFieldValue('COLUMN_KEY');
            $orderBy = $o->GetFieldValue('ORDER_BY');

            $column = $orderByCfg[$colKey];
            $order = $orderMap[$orderBy];

            array_push($clause, "$column $order");
        }

        $str = 'ORDER BY ' . implode(', ', $clause);

        $model->OverideOrderBy($ind, $str);

        return;
    }    
}

?>
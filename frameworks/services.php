<?php
/* 
    Purpose : Custom services list
    Created By : Seubpong Monsar
    Created Date : 09/04/2017 (MM/DD/YYYY)
    IBSVer : 1.0 
*/

require_once "phar://wis_sass_framework.phar/sass_framework_include.php";

$bp = "phar://wis_sass_framework.phar/";

$userLvl = ['WhoCanRun' => 'user',    'Channel' => 'all'];
$admnLvl = ['WhoCanRun' => 'admin',   'Channel' => 'all'];
$initLvl = ['WhoCanRun' => 'nologin', 'Channel' => 'cli'];

$ONIX_SERVICES_LIST = [
    #Event
    'SassGetEventList' => [$bp . 'controllers/Monitoring/Event.php', 'SassGetEventList', $userLvl],
    'SassGetEventInfo' => [$bp . 'controllers/Monitoring/Event.php', 'SassGetEventInfo', $userLvl],
    'SassCreateEvent' => [$bp . 'controllers/Monitoring/Event.php', 'SassCreateEvent', $userLvl],
    'SassUpdateEvent' => [$bp . 'controllers/Monitoring/Event.php', 'SassUpdateEvent', $userLvl],
    'SassDeleteEvent' => [$bp . 'controllers/Monitoring/Event.php', 'SassDeleteEvent', $userLvl],   
    
    #Search Filter    
    'SassGetSearchTextList' => [$bp . 'controllers/General/SassSearchFilter.php', 'SassGetSearchTextList', $userLvl], 

    #Micro Service
    'SassGetMicroServiceList' => [$bp . 'controllers/Sass/MicroService.php', 'SassGetMicroServiceList', $userLvl],
    'SassGetMicroServiceInfo' => [$bp . 'controllers/Sass/MicroService.php', 'SassGetMicroServiceInfo', $userLvl],
    'SassCreateMicroService' => [$bp . 'controllers/Sass/MicroService.php', 'SassCreateMicroService', $userLvl],
    'SassUpdateMicroService' => [$bp . 'controllers/Sass/MicroService.php', 'SassUpdateMicroService', $userLvl],
    'SassDeleteMicroService' => [$bp . 'controllers/Sass/MicroService.php', 'SassDeleteMicroService', $userLvl],
    'SassIsMicroServiceExist' => [$bp . 'controllers/Sass/MicroService.php', 'SassIsMicroServiceExist', $userLvl],    

    #Instance
    'SassGetInstanceList' => [$bp . 'controllers/Sass/Instance.php', 'SassGetInstanceList', $userLvl],
    'SassGetInstanceInfo' => [$bp . 'controllers/Sass/Instance.php', 'SassGetInstanceInfo', $userLvl],
    'SassCreateInstance' => [$bp . 'controllers/Sass/Instance.php', 'SassCreateInstance', $userLvl],
    'SassUpdateInstance' => [$bp . 'controllers/Sass/Instance.php', 'SassUpdateInstance', $userLvl],
    'SassDeleteInstance' => [$bp . 'controllers/Sass/Instance.php', 'SassDeleteInstance', $userLvl],
    'SassIsInstanceExist' => [$bp . 'controllers/Sass/Instance.php', 'SassIsInstanceExist', $userLvl],

    #Disk Image
    'SassGetDiskImageList' => [$bp . 'controllers/Sass/DiskImage.php', 'SassGetDiskImageList', $userLvl],
    'SassGetDiskImageInfo' => [$bp . 'controllers/Sass/DiskImage.php', 'SassGetDiskImageInfo', $userLvl],
    'SassCreateDiskImage' => [$bp . 'controllers/Sass/DiskImage.php', 'SassCreateDiskImage', $userLvl],
    'SassUpdateDiskImage' => [$bp . 'controllers/Sass/DiskImage.php', 'SassUpdateDiskImage', $userLvl],
    'SassDeleteDiskImage' => [$bp . 'controllers/Sass/DiskImage.php', 'SassDeleteDiskImage', $userLvl],
    'SassIsDiskImageExist' => [$bp . 'controllers/Sass/DiskImage.php', 'SassIsDiskImageExist', $userLvl],

];

CDispatcher::RegisterServices($ONIX_SERVICES_LIST);

?>
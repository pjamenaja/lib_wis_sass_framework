<?php

$buildFile = 'wis_sass_framework.phar';

$srcRoot = './frameworks';
$buildRoot = './build';
$testDir = './test';

print("Creating build.php ...\n");

$fname = "$srcRoot/build.php";
$oh = fopen($fname, "w") or die("Unable to open file [$fname]!");
$dt = date("m/d/Y h:i:sa"); //date("m/d/Y");
$tmp = '$WIS_SASS_FRAMEWORK_BUILT_DATE = ' . "'$dt'";

$stmt = <<<EOD
<?php
/* 
Purpose : Auto generated file (DO NOT MODIFY)
Created By : Seubpong Monsar
*/

$tmp;

?>
EOD;

fwrite($oh, $stmt);
fclose($oh);


$phar = new Phar($buildRoot . "/$buildFile", 0, $buildFile);

print("Starting building [$buildFile] to directory [$buildRoot]\n");

$phar->buildFromIterator(
    new RecursiveIteratorIterator(new RecursiveDirectoryIterator($srcRoot, FilesystemIterator::SKIP_DOTS)),
    $srcRoot);

//copy("onix_core_framework.phar", "$buildRoot/onix_core_framework.phar");
//copy("$buildRoot/$buildFile", "D:/wis_projects/scm/$buildFile");

print("Building file DONE\n");

?>